Prepare_cobramm_input
---------------------

Preparation of the initial files can be a tedious procedure depending on
the complexity of the system under investigation. One has to define the
*High*, *Medium* and *Low* layers, specify atom-links, redistribute
charges in residues which appear at the boundary between *High* and
*Medium,* as well as between *High* and *Low* layers, etc. The
*Prepare\_cobramm\_input.pl* script performs these activities
automatically relying on a user submitted parameter file *cobram.parm*.

# 1 Features in a glance

#### Automatic inclusion of residues (including solvent) into the High (H) or Medium (L) layer

1.  participating in H-bonds with heteroatoms from the H and the M layer
2.  which are within a user-defined distance from the center of mass of
    the H layer OR of the H and M layers (selection within a sphere
    around the QM)
3.  which are within a user-defined distance from the H layer atoms OR
    from the H and M layer atoms (selection following the shape of the
    QM)

####  Automatic generation of the initial conditions for running QM/MM dynamics simulations

1.  Zero point energy (ZPE) sampling utilizing Gaussian with mode freezing
2.  Wigner sampling at finite temperature with mode freezing
3.  Heating and equilibration of the sampled geometries utilizing Amber
    10 or later
4.  Extraction of velocities for movable atoms
5.  Preparation of the !rattle ?rattle block for constraining TIP3P waters

####  Force field parameters

1.  Use of all force fields supplied in the Amber libraries
2.  Basic functionality for automatic generation of non-standard
    residues (using Amber's *antechamber* and *parmchk* utilities and
    charges obtained with semi-empirical methods like *bcc*)
3.  Pre-defined non-standard residues and libraries supplied by the user
    can be directly incorporated in the procedure for generating the
    starting files

####  Miscellaneous 

1.  Charge redistribution in the M and L layers (necessary for having an
    integer charge in each layer) in case of covalent bonds at the H/M
    or H/L interface
2.  Sorting of the M and L layer atoms (can be used for extracting
    solvation shells)
3.  Generation of files with the Cartesian coordinates of the *High*
    (HighLayer.xyz) as well as of the *High* and *Medium* layer
    (HighMediumLayer.xyz) atoms.

# 2. Prerequisites

* **Amber 10 (or later)**: export the variable AMBERHOME to the main 
    Amber folder (e.g. /usr/local/amber18) in your bash shell before 
    executing the script. *Earlier versions of Amber do not work.*

* **Gaussian 03 (or later)**: required for sampling of the initial 
    conditions for running QM/MM dynamics; if your version of GAUSSIAN 16 
    is in /usr/local/ export the variables GAUSSIAN_DIR (e.g. /usr/local/) 
    and GAUSSIAN_EXE (e.g. g16)

* **COBRAMM**: required for sampling of the initial conditions for 
    running QM/MM dynamics; export the variable COBRAMM_PATH to the 
    folder with COBRAMM's scripts (e.g. /usr/local/cobramm-git/cobramm)

* **MPI**: optional for speeding up heating and equilibration of the 
    sampled geometries (in case of a Low layer); export the variable 
    MPIHOME (e.g. /usr/local/amber18/AmberTools/src/mpich2-1.4/instlld/bin)
    Note that the script wil automatically detect the number of CPUs 
    on your computer to use

# 3. Input files

## 3.1 The `cobram.parm`

The cobram.parm contains all the information necessary to generate the
COBRAMM input files *real\_layers.xyz, real.top, model-H.top* and
*velocity.dat* (latter required only in case of dynamics simulations).
A commented template of the cobram.parm input file is available in the 
repository. 

## 3.2 The `<name>.pdb`

A geometry file is required for **VARIANT 1** and **VARIANT 3** (see
Chapter 5 for details regarding the **VARIANTS**).

The .pdb file must satisfy the following conditions:

1.  The names *real.pdb* and *model-H.pdb* are internally used and
    should be avoided.

2.  In case of non-standard residues the atom names (3^rd^ column) must
    be specified in the following format (C, C1, C2, N, N1, N2, etc.)

3.  Three-letter codes should be used for non-standard residue names
    (4^th^ column)

4.  To avoid undesired collisions one should use non-standard residue
    names which differ from the names defined in the used MM force
    fields. Avoid using names AAA through ZZZ as they are currently
    used by the script.

5.  Solvent atoms should come last.

As a rule one should test the .pdb with Amber's *tleap* or *xleap*
utility for missing atoms, non-integer charges, wrong/missing
connectivity. Beware that information about S-S bonds between CYX
residues is not contained in the .pdb file, therefore it must be
specified in the *cobram.parm*. Note, that .pdb files prepared with
different MD programs may have different naming convention for some
residues (e.g. CYS, CYX). Be alert that in the process of generation of
the input files the order of the residues (due to grouping of all Medium
layer residues) as well as the order of atoms within a residue (due to
comparing towards the Amber libraries) may be altered and *the atom
order in *real\_layers.xyz* may differ from the atom order in the
provided .pdb file*. The ordering in the .top files will be consistent
with the ordering in the *real\_layers.xyz*.

## 3.3 The `geometry.chk`, `geometry.fchk`, `geometry.chk.gz`

The Gaussian checkpoint file containing the information from the
frequency calculation is used for the sampling procedure, i.e. **VARIANT
2**.

## 3.4 The `<name>.in`

An Amber command file is used as template for heating and equilibration
in case of non-0K QM/MM dynamics in **VARIANT 2**.

## 3.5 The `<name>.top`

An Amber topology file is used as template for heating and equilibration
in case of non-0K QM/MM dynamics in **VARIANT 2**.

## 3.6 The `<name>.crd`

An Amber .rst output file containing Cartesian coordinates and
velocities can be specified for fetching the velocities of the Low layer
in **VARIANT 2** *only if* all QM/MM computations based on the MD
snapshot were performed in with the **H**(igh)**L**(ow) scheme.
Requires *crd\_2\_HM.map.*

## 3.7 The `crd_2_HM.map`

Produced by the script together with *real\_layers.xyz. real.top* and
*model-H.top* during **VARIANT 1**. The *crd\_2\_HM.map* file *is not
required* by COMBRAMM but by *Prepare\_cobramm\_input.pl* for running
**VARIANT 2** when a &lt;name&gt;.crd is provided and contains
information about the ordering of the H and M layer atoms, as the
ordering of the atoms in the *real\_layers.xyz* can differ from the
order of the supplied .pdb file.

## 3.8 The `real_layers.xyz`

If MD velocities are *not* available for the snapshot (e.g. because they
were not stored originally or if the preceding QM/MM energy minimization
of the MD snapshot was performed in the
**H**(igh)**M**(edium)**L**(ow) scheme) the &lt;name&gt;.crd will be
automatically created from the supplied *real\_layers.xyz* file.
Required for **VARIANT 2** when &lt;name&gt;.crd is not provided.

## 3.9 `velocity.dat`

Produced by the script from the last structure in the equilibration in
**VARIANT 2**, required in **VARIANT 3** for the 2^nd^ step of the
generation of the initial conditions.

## 3.10 Non-standard residues

Users can provide non-standard residue libraries in **VARIANT 1** and
**VARIANT 3**. Required are a) a file with atom names – atom types
mapping & charges (.in, .mol2, .lib) and b) a file with additional
parameters (.frcmod)

# 4. Execution

The script does not need to be compiled. It can be executed directly
from the console.

    ./Prepare_cobramm_input.pl 1>prep.log 2>prep.err &

# 5. Work flow

There are two execution variants:

#### **VARIANT 1**
Input files will be generated for using COBRAMM with
keyword “type“ set to optxg(p), ts(p), irc(p), ci(p) or freqxg(p).

#### **VARIANT 2** and **3** 
A two-step procedure for generating the input
files for using COBRAMM with the cobram.command keyword type set to
mdv(p).

**VARIANT 1** and **3**:
1. Standard residues are loaded from the Amber libraries, user-supplied
per-defined non-standard residue libraries are loaded from working
directory and tested, non-standard residues for which there are no
parameters are created.
2. Coordinate file .pdb is reordered if the atom order inside the
residues differs from the order in the provided libraries. The original
.pdb is copied to .pdb\_orig.
3. H, M and L layers are defined according to the user input.
4. L layer molecules are moved from to either H or M layer according to
a user-specified scheme (see *automatic inclusion of residues to High
and Medium layer*).
5. Link atoms are identified
6. The *real\_layers.xyz* and *crd\_2\_HM.dat* are prepared. Note, that
the atom order in the *real\_layers.xyz* and .pdb may differ as all
movable (i.e. M layer) solvent molecules are grouped together. Frozen
(i.e. L layer) solvent molecules come last. *crd\_2\_HM.dat* maps the
new positions to the original .pdb positions.
7. The *model-H.top* is created via tleap using the .lib files of all
involved standard residues, as well as .lib and .frcmod files for all
non-standard residues. Charges are set to zero as electrostatics will be
treated through the QM-software.
8. The *real.top* is created is created via tleap using the .lib files
of all involved standard residues, as well as .lib and .frcmod files for
all non-standard residues. If link atoms are present, charges in the
residues which are shared between H an M layer are redistributed so that
the charge of each layer is integer and for each residue QM charge +
residual MM charge = total MM charge. The charge of the QM region is set
to zero.
9. The types of the common atoms to real.top and model.top are compared.

Only in **VARIANT 1**:

10. The *cobram.parm, real\_layers.xyz, real.top, model-H.mol2,
crd\_2\_HM.map, HighLayer.xyz* and *HighMediumLayer.xyz* are stored to
the directory cobramm\_files.

Only in **VARIANT 3**:

10. The velocity.dat is edited according to the H/M layer selection,
removing velocities of all L layer atoms. A 0.0 0.0 0.0 line is added
for each link atom at the end of the file.

11. If Water (TIP3P WAT) or Chloroform (CL3) molecules are included in the M layer the !rattle
section of the *cobram.command* file is generated (see Chapter XX). This
is required as the solvent force fields are normally parametrized only for
equilibrium geometries. Through the rattle routine one allows each M layer solvent molecule to move as
a whole but not to vibrate.
12. The *cobram.parm, real\_layers.xyz, real.top, model-H.mol2,
velocity.dat, HighLayer.xyz* and *HighMediumLayer.xyz* are stored to the
directory cobramm\_files.

**VARIANT 2**: (This variant provides only intermediate files, which
must be processed further in **VARIANT 3**)

Looping over the number of initial conditions \$nGeom is initiated.
Within each iteration:

#### **A.** If a MM geometry/velocity (.crd) file **was not** provided (implies no MD velocities available):

1. A .crd file is automatically created out of the real\_layers.xyz.
2. A directory geom\_&lt;001...\$nGeom&gt; is created. All necessary
files are copied from the parent directory.
3. According to the user's choice either Wigner (default) or zero point 
energy (ZPE) is performed; Wigner sampling is done via COBRAMM whereas 
ZPE sampling is done by Gaussian; in both cases the a *geometry.chk* 
from a previous frequency calculation is required.
4. The H/M layer coordinates in MM .crd snapshot are replaced with the
ones generated through the sampling procedure.
5. Heating of the sample in 5 ps stages of 50 K (time step 1 fs) with
constraints on the H/M layer atoms (either harmonic through ntr = 1,
Restrains, 100 kcal/mol or ibelly) is performed. NO periodic conditions
are used. Either Langevin (with harmonic constraints) or Anderson (with
ibelly) thermostat are used.
6. Equilibration of the L layer atoms is done for 10 ps (time step 2
fs) with constraints on H/M layer atoms (either harmonic through ntr =
1, Restrains, 100 kcal/mol or ibelly). NO periodic conditions are used.
Either Langevin (with harmonic constraints) or Anderson (with ibelly)
thermostat are used.

As with harmonic constraints the atoms are not completely frozen and
vibrate a bit at the end of the equilibration the H/M layer coordinates
in the final MM (.rst) snapshot are replaced with the sampled ones. In
case of ibelly this is not necessary as the High and Medium atoms are
fixed.

7. Re-imaging is performed with respect to the H and M layer
coordinates and a geom\_&lt;001...\$nGeoms&gt;.pdb file is created.
A8. Finally, the directory input/ is created and the
geom\_&lt;001...\$nGeoms&gt;.pdb, *velocity.dat* and *cobram.parm* are
copied therein. The proper .pdb name is inserted in the “coordinate
file” line of *cobram.parm*.

At this stage the user is given the possibility to modify the .pdb file
and to re-define the H/M/L layers. *Beware that in the case of
non-standard residues the .pdb will contain some arbitrary residue and
atom names, which have to be corrected manually* (remember that the
naming convention for non-standard residues atoms is C, C1, C2, etc.

In order to obtain the final input files for QM/MM dynamic simulations
*the script should be run once more* in the directory *input* (**VARIANT
3**).

#### **B.** If a MM geometry/velocity (.crd) file **was** provided:

1. A directory geom\_&lt;001...\$nGeom&gt; is created. All necessary
files are copied from the parent directory.
2. According to the user's choice either Wigner (default) or zero point 
energy (ZPE) is performed; Wigner sampling is done via COBRAMM whereas 
ZPE sampling is done by Gaussian; in both cases the a *geometry.chk* 
from a previous frequency calculation is required.
3. The H/M layer coordinates in MM .crd snapshot are replaced with the
ones generated through the sampling procedure. The H/M layer atom
velocities are set to 0.0 0.0 0.0.
4. Equilibration of the L layer atoms is done for 10 ps (time step 2
fs) with constraints on H/M layer atoms (either harmonic through ntr =
1, Restrains, 100 kcal/mol or ibelly). NO periodic conditions are used.
Either Langevin (with harmonic constraints) or Anderson (with ibelly)
thermostat are used.

As with harmonic constraints the atoms are not completely frozen and
vibrate a bit at the end of the equilibration the H/M layer coordinates
in the final MM (.rst) snapshot are replaced with the sampled ones. In
case of ibelly this is not necessary as the High and Medium atoms are
fixed.

5. Re-imaging is performed with respect to the H and M layer
coordinates and a geom\_&lt;001...\$nGeoms&gt;.pdb file is created.
6. Finally, the directory input/ is created and the
geom\_&lt;001...\$nGeoms&gt;.pdb, *velocity.dat* and *cobram.parm* are
copied therein. The proper .pdb name is inserted in the “coordinate
file” line of *cobram.parm*.

At this stage the user is given the possibility to modify the .pdb file
and to re-define the H/M/L layers. *Beware that in the case of
non-standard residues the .pdb will contain some arbitrary residue and
atom names, which have to be corrected manually* (remember that the
naming convention for non-standard residues atoms is C, C1, C2, N, N1,
N2, etc.

In order to obtain the final input files for QM/MM dynamic simulations
*the script should be run once more* in the directory input (**VARIANT
3**).

# 6. Output

The output file begins with information what type of inputs will be
prepared
* **VARIANT 1**: Input files will be generated for running optimizations
with COBRAMM
* **VARIANT 2**: Intermediate files for running dynamics with COBRAMM will
be generated.
*  **VARIANT 3**: Input files for running dynamics with COBRAMM will be
generated.

**VARIANT 1 & 3**:

First the force field files are read for equivalent atom names

    List of equivalent notations:
    O5\* O5'
    C5\* C5'
    O4\* O4' O1'

Non-standard residues are generated if requested

    1 non-standard residues will be generated
    Name Type Charge Model
    LIG  gaff   0     bcc
    Generating non-standard residue 0 ...
    antechamber -fi pdb -i LIG.pdb -fo mol2 -o LIG\_out.mol2 -at gaff -nc 0 -c bcc
    parmchk -i LIG\_out.mol2 -f mol2 -o LIG.parm -p \$AMBERHOME/dat/leap/parm/gaff.dat
    done!

otherwise

    Only standard residues used throughout.

Next, if predefined residues have been specified, they are checked with
*tleap* for consistence

    Checking predefined residue 0 ...done!

Finally, standard residues are loaded.

    Loading library for residue ...

A comparison between the atom order of the residues in the supplied .pdb
and in the Amber .lib files is performed. *If the ordering differs the
.pdb file is reordered.*

    Loading library for residue THR.
    Atom name H1 of residue GNT could not be found among atom names CA. Order in the .pdb
    and .lib differs.

Next, the *reordered* &lt;name&gt;.pdb file is read

    Reading &lt;name&gt;.pdb ... done!

Next, the user is informed of the automatic selection of H/M residues
scheme

    Residues WAT PHE TRP within 4.5 from CoMH (default) / CoMHM / DtH / DtHM will be included.

OR

    H-bonded residues WAT PHE TRP will be included to H / M (default) layer.

If CoMH or CoMHM are chosen as options the center of mass of the H / H+M
atoms is printed

    Center of mass: -3.11501850195823 10.4011700655446 -0.913626942304209

If H-bonded residues are selected the following output is printed

    Looking for H-bonds ... done for heteroatoms in the high/medium layer (Het(H/M)---H-Het(M/L))
    Looking for H-bonds ... done for H's attached to heteroatoms in the high/medium layer
    (Het(H/M)-H(H/M)---Het(L)

Next, the number of molecules which will be added to the H/M layer is
listed

    Number of molecules that will be added to the high / medium layer: 112

Next, the H and M layer atoms are listed with the residues they belong
to:

    High layer atoms: 4744(RET) 4745(RET) 4746(RET) 4747(RET) …
    Medium layer atoms: 1240(ASN) 1241(ASN) 1242(ASN) 1243(ASN)

Next, the link atoms are listed

    Link atoms: H      M
            4744(C) 4741(C)

The real\_layers.xyz is created. Sorting is performed to group all M
layer solvent residues together

    Generating the real\_layers.xyz and performing H/M/L layer sorting ... done!

The model-H.top is created.

    Generating model-H.top ... done!

If a residue is shared between the H and the M or L layers (implies the
H-link technique) charges have to be redistributed among the M / L atoms
of the residue so that the total charge (QM charge + MM charge) of the
residue remains an integer. The residues with modified charges get new
names (AAA, BBB, etc.)

    Reassigning charges for residue: RET -&gt; AAA …

    QM charge: +1 

(the QM charge of RET)

    Total charge the residue should have: 1.00000 

(the total QM+MM charge of
retinal, thus, the residual MM charge of the RET atoms in the M / L
layer must be 0.0)

    Total charge the MM atoms have before redistribution of the residual charge: 0.120275 
    
(the charge which the M/L layer atoms of RET have
before redistribution → not 0.0 as required)

    Total residual charge to redistribute among MM atoms: -0.120270
    Total charge the MM atoms have after redistribution of the residual charge: 0.00000
    
(the charge of the M/L layer atoms from the retinal
residue after redistribution)

    ... done!

The real.pdb is created.

    Building real.pdb file ... done!

Next, a comparison of the atom types in model-H.top and real.top is
performed.

    Comparing atom types in real.top and model-H.top ...
    Atom types in real.top : CT H1 H1 NT H CM HA CD HA CD CT HC HC HC CD HA
    Atom types in model-H.top: CT H1 H1 NT H CM HA CD HA CD CT HC HC HC CD HA AL

Note, that the model-H.top may contain pseudo atom types 'AL' for link
atoms. These atom type carry no force field parameters. Link atoms are
not present in the real.top.

Only in **VARIANT 3**:

Next, the velocity file is edited leaving only the velocities of the
movable (H+M) atoms

    Editting velocity.dat … done!

In case of TIP3P water as solvent the RATTLE input is generated

    Generating rattle … done!

otherwise a worning is printed

    Rattle implemented only for TIP3P water (WAT) residues.

**VARIANT 1 & 3**:

At the very end the user is informed that the relevant files are stored.

    Saving files to directory cobramm\_files!

**VARIANT 2** (output in the case when *no* .crd file is provided)
COBRAMM informs that initial conditions will be generated (e.g. via Wigner sampling)

    Initial conditions for running dynamics with Cobram will be generated.
    Initial conditions will be generated by Wigner sampling.
    Read checkpoint file geometry.chk
    Write formatted file geometry.fchk

The number of normal modes is reported 

    Number of normal modes of the High(+Medium) Layer = 54

After that a loop over the number of initial conditions is initiated.
Within each iteration following output is written

    ##################################################
    Proceeding geometry 1 ...

    Highest active mode: 46
    Inactive modes: 47 48 49 50 51 52 53 54

Thereby, the list of inactive (frozen) modes is also reported.
Next, the sampled Cartesian coordinates (in Angstrom, shifted back from
the center of mass to original position) are read out of the gaussian
ouptut file.

    XYZ:
    N: 15.677213402616 15.5372647117321 17.8108333147099
    C: 16.8969360659764 15.5658937042786 18.4250611219387

Next, the sampled velocities (in Bohr/au) are read out of the gaussian
output file.

    Veloc:
    N: -0.000491215325029186 0.000458596912078654 0.000180627504351079
    C: 5.57174370298071e-05 -0.000248707543412282 -0.000272436918531448

Heating is performed in 5 ps stages of 50 K.

    Heating the system to 300K in 6 stages of 5 ps ...
    No periodic boundery conditions will be used! Harmonic restrains (100 kcal/mol) will be used to freeze the H and M layer atoms.

    Alternatively, H and M freezing through ibelly can be used. For this
    option set \$constrain = 1 in the script. Beware, that Langevin dynamics
    is incompatible with ibelly = 1 and ntt = 2 (Anderson coupling scheme) 
    will be used instead.

    \$MPIHOME/mpirun -np 4 \$AMBERHOME/exe/sander.MPI -O -i heat0-50.in -p
    GMP.top -c real.crd -r heat0-50.rst -o heat0-50.out -ref real.crd
    \$MPIHOME/mpirun -np 4 \$AMBERHOME/exe/sander.MPI -O -i heat50-100.in -p
    GMP.top -c heat0-50.rst -r heat50-100.rst -o heat50-100.out -ref real.crd

Finally, equilibration is performed for 10 ps

    Equilibrating system for 10ps (using 4 cores) ...
    No periodic boundery conditions will be used! Periodic restrains (100
    kcal/mol) will be used to freeze the H and M layer atoms.
    Alternatively, H and M freezing through ibelly can be used. For this
    option set \$constrain = 1 in the script. Beware, that Langevin dynamics
    is incompatible with ibelly = 1 and ntt = 2 (Anderson coupling scheme)
    will be used instead.

    Equilibrating system for 10ps (using 4 cores) ...
    \$MPIHOME/mpirun -np 4 \$AMBERHOME/exe/sander.MPI -O -i GMP\_100ns.in -p
    GMP.top -c heat250-300.rst -r equilibration.rst -o
    equilibration\_100ps.out -x equilibration.mdcrd -ref heat250-300.rst
    done!

At the end of each iteration a .pdb and velocity.dat files are created

    Generating a .pdb file ... done!
    Generating velocity.dat ... done!

At the end of the loop over the realizations analysis of the sampling procedure is provided; 
Note that for a reliable sampling all four values should be similar

    ######## ANALYSIS OF THE WIGNER SAMPLING #########
    Energy in all active modes from frequency calculation:    4.73 eV
    Average kinetic energy in all active modes from Wigner sampling:    4.73 eV
    Average potential energy in all active modes from Wigner sampling:    4.70 eV
    Average kinetic energy in all active modes from velocities:    4.73 eV

# 7. Error message handling

The *Prepare\_cobramm\_input.pl* relies heavily on
Amber and Gaussian. The complexity of these programs makes their
handling prone to errors. Therefore, the user is asked to be critical
against the final results. One has following possibilities to verify the
correctness of the input files.

1. Examine the output file of the preparation procedure (*prep.log*).
The script checks for availability of files and for the consistency of
the generated files. Currently about 50 self explanatory error messages
and warnings have been implemented which are printed in red or yellow,
respectively (when reading the file from the console with *less*)

2. Examine the *leap\_\*.log* files created during the generation of
residue libraries and topologies. Missing force field parameters,
unspecified residues, undefined atom types, added hydrogens and heavy
atoms will be listed here. Warnings regarding non available improper
torsion parameters can be ignored.

3. Look at the .parm files for “*ATTN, need revision”* warnings. A
*model-H.parm* must be present in the working directory and If there are
link-atoms ONLY against parameters containing an **AL** atom type there
should be written *ATTN, need revision*

    H1 - CT - **AL** 0.00 0.000 ATTN, need revision

Normally, if a force field parameter is missing tleap will strike and
will refuse to produce the requested .top files. However, if parmchk is
run over the geometry for obtaining missing parameters and fails the
generated .parm file will contain an “*ATTN, need revision*” line
against each missing parameter. When this .parm file is provided to
tleap (using the tleap command loadamberparams) tleap is happy and will
ignore the missing parameter (more precisely the .top file will contain
0.0 for the parameter). Therefore, make sure that all non-standard
residues have “*ATTN, need revision*”-free .parm files.

4. Examine the error file (prep.err). Errors here may point at errors in
the script itself and should be reported.

5. When preparing the initial conditions for dynamics simulations
(VARIANT 2) check the Amber input and output files. The script takes the
provided Amber input file and edits it to suit it's purposes (e.g.
adding restrains). If something goes wrong Amber would print an error
message in the output.

6. When preparing the initial conditions for dynamics simulations
(VARIANT 2) check the intermediately generated geom\_&lt;n&gt;.pdb files
(n = 001...\$nGeoms) for consistency.

7. When preparing the initial conditions for dynamics simulations always 
inspect the section ######## ANALYSIS OF THE WIGNER SAMPLING ######### 
and the possible error messages. For a reliable sampling all four values 
should be similar.

# 8. Q&A

1. **How do I add to the H layer solvent molecules involved in hydrogen
bonds with the H layer?**

automatic inclusion of residues to High and Medium layer = WAT H

2. **I don't want to include any residue neither in the H nor in the M
layer. How do I do that?**

Leave the line empty!

automatic inclusion of residues to High and Medium layer =

3. **How do I add to the M layer solvent molecules in a radius of 5.3 A
around the center of mass of the H layer?**

automatic inclusion of residues to High and Medium layer = WAT 5.3 CoMH

4. **How do I add to the M layer all residues in a radius of 5.3 A around
the center of mass of the H layer?**

automatic inclusion of residues to High and Medium layer = WAT ALL 5.3
CoMH

5. **How do I add to the M layer all residues ALA and TYR (but not
solvent) in a distance of 3.5 A from any residue in the H and M layer?**

You can't do it directly! But you can try following two-step procedure:
1\. Specify automatic inclusion of residues to High and Medium layer =
WAT ALA TYR 3.5 DtHM and run the script
2\. Change the M label against the solvent molecules in the
*real\_layers.xyz* to L.

6. **How do I add to the H layer solvent molecules in a radius of 5.3 A
around the center of mass of the H layer?**

You can't do it directly! But you can try following two-step procedure:
1\. Specify solvent = WAT 5.3 CoMH and run the script. This will produce
a real.pdb file in which all solvent molecules within 5.3 A will be
grouped together. The *real\_layers.xyz* will tell you which these atoms
are.
2\. Use the *real.pdb* file as the input file to run the script. Specify
explicitly which atoms you want to have in the H layer. Note that you
would have to hand-edit the *real.pdb* (residue numbering) file and
possibly replace atom and residue (AAA, BBB) names.

7. **What parameters are used for creating non-standard residues?**

The script assigns either *amber* and *gaff* (default) atom types
(specified through non-standard residues atom types) and checks for
missing parameters against parameter files specified in non-standard
residues parameters (default is *gaff.dat*).

8. **I have a non-standard library (.lib) and a .frcmod parameter file.
How do I make the script to consider them?**

There are two possibilities:
1\. Place the .lib and .frcmod files in the directory where you execute
the script. Add pre-defined non-standard residues = RET RET.lib
RET.frcmod to *cobram.parm* and run the script.
2\. Copy RET.lib to \$AMBERHOME/dat/leap/lib/. and RET.frcmod to
\$AMBERHOME/dat/leap/parm/. Add

    loadOff RET.lib
    loadamberparams RET.frcmod

to the force field you plan to use. In this way the non-standard residue
becomes standard.

9. **I have a non-standard library (.lib) but I don't have an .frcmod.
What can I do?**

Create an “empty” .frcmod file with the following content

    remark goes here
    MASS
    BOND
    ANGLE
    DIHE
    IMPROPER
    NONBON

and then specify the name of the .frcmod file as the 3^rd^ value in
non-standard residues parameters.

10. **What files do I need to generate the initial conditions for QM/MM
optimization?**

1\. The *cobram.parm* file (see Chapter 3.1)
2\. The .pdb file (please read the restrictions on the .pdb format,
Chapter 3.2)

11. **What files do I need to generate the initial conditions for QM/MM
dynamics simulation?**

1\. The *cobram.parm* file (see Chapter 3.1).
2\. The *geometry.chk* file from a previous frequency calculation (see
Chapter 3.3).
3\. The MD input (.in) file which was used in the production of the MM
snapshot (see Chapter 3.4).
4\. The MD topology (.top) which was used in the production of the MM
snapshot (see Chapter 3.5).
5\. The original MM (.crd) snapshot (i.e. before the QM/MM refinement) if
velocities are avialable (see Chapter 3.6).
6\. if a .crd file is present, the *crd\_2\_HM.dat* generated during the
first execution of the script (see Chapter 3.7).
7\. if a .crd file is *not* present the *real\_layers.xyz* generated
during the first execution of the script (see Chapter 3.8).

12. **How do I use older force fields?**

First check in the \$AMBER/dat/leap/cmd/oldff directory. If the force
filed is there you can directly request it in the cobram.parm

    force fields = oldff/leaprc.rna.ff99

Otherwise copy the force field file in \$AMBER/dat/leap/cmd. Don't
forget to provide the paramter files required by the force field to
\$AMBER/dat/leap/parm.

13. **I don't have MM velocities (I've stored only the Cartesian
coordinates during the MD) but want to generate the initial conditions
for QM/MM dynamics simulations. What do I do?**

If no velocities are available the script will first heat the QM/MM
refined snapshot and subsequently will run equilibration. The
real\_layers.xyz file must be provided in this case.

14. **If I have used H/M/L partitioning during the frequency calculations,
how are the velocities of the M layer atoms generated?**

The velocities of the M layer atoms are generated in the sampling
procedure. Gaussian does not make a difference between H and M layer
atoms. It should be pointed out that having many molecules in the M
layer (e.g. solvent molecules) will lead to instabilities because of
imaginary or near-zero frequencies. Additionally, Gaussian, will distort
both H and M layer atoms from their equilibrium positions during the
sampling, thus also solvent molecules. The TIP3P force field cannot
handle non-equilibrium waters. Therefore, users are discouraged to do
this (read Q&A 15).

15. **What is the best procedure for generating the initial conditions for
QM/MM dynamic simulations if I want to include water molecules in the M
layer.**

Two possibilities exist:
1\. Run QM/MM refinement with a H/M/L partitioning, adding waters to the
M layer. Note that as the positions of the movable waters have changed
with respect to the original MM snapshot their velocities cannot be used
and we need to provide *real\_layers.xyz*.
2\. In the *real\_layers.xyz* change the labels of the M layer waters
from 'M' to 'L' and perform a frequency calculation. In this way you
will avoid problems with near-zero or imaginary frequencies, as well as,
problems arising from the sampling of M layer water molecules, which
cannot be handled with TIP3P.
3\. Provide the edited *real\_layers.xyz* and use the script to generate
velocities for the H layer (w/o waters), as well as, to heat and
equilibrate the sample.
4\. Before running the *Prepare\_cobramm\_input.pl* again edit the
*cobram.parm* file and edit the line automatic inclusion of residues in
the High and Medium layer to include waters in the M layer according to
one of the available schemes. Their velocities are obtained from the MM
equilibration. The M layer waters are equilibrium waters, hence they can
be handled with the TIP3P force field via RATTLE.

Alternatively:
1\. Run QM/MM refinement with a H/L partitioning (no M layer) w/o waters
in the H layer.
2\. Perform a frequency calculation.
3\. Provide the *crd\_2\_HM.dat* file (see Chapter 2.2.3.7) and the .crd
file with the Cartesian coordinates of the MD snapshot and its
velocities (see Chapter 2.2.3.6). Run the script to generate velocities
for the H layer (through the sampling procedure) and, subsequently, to
equilibrate the system. Since the waters were always in the L layer and
never moved, their velocities (stored in the .crd file) are used to
start the equilibration and no heating is required.
4\. Before running the *Prepare\_cobramm\_input.pl* again edit the
*cobram.parm* file and edit the line automatic inclusion of residues in
the High and Medium layer to include waters in the M layer according to
one of the available schemes (see Chapter). Their velocities are
obtained from the MM equilibration. The M layer waters are equilibrium
waters, hence they can be handled with the TIP3P force field via RATTLE.
